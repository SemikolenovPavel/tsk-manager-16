package ru.t1.semikolenov.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showErrorArgument(String arg);

    void showErrorCommand(String arg);

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

}
