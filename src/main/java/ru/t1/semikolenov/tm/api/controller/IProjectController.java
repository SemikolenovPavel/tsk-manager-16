package ru.t1.semikolenov.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProjects();

    void createProject();

    void removeProjectByIndex();

    void removeProjectById();

    void showProjectByIndex();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

}
