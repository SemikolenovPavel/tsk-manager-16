package ru.t1.semikolenov.tm.repository;

import ru.t1.semikolenov.tm.api.repository.IProjectRepository;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

    @Override
    public Project remove(Project project) {
        projects.remove(project);
        return project;
    }

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void clear() {
        projects.clear();
    }


    @Override
    public Project findOneById(String id) {
        for (final Project project: projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(Integer index) {
        return projects.get(index);
    }

    @Override
    public boolean existById(final String id) {
        return findOneById(id) == null;
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public int getSize() {
        if (projects == null || projects.isEmpty()) return 0;
        return projects.size();
    }
}
